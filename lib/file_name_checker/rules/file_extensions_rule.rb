require_relative '../rule'

module FileNameChecker
  class FileExtensionsRule < Rule
    def check(path)
      if FileTest.directory?(path)
        true
      else
        @settings.include?(path.split('.', 2)[-1])
      end
    end
  end
end
