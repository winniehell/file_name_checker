require_relative '../rule'

module FileNameChecker
  class SnakeCaseRule < Rule
    def check(path)
      if FileTest.directory?(path)
        /\/[a-z0-9_]+\z/.match(path)
      else
        /\/[a-z0-9_]*(\.[a-z0-9]+)+\z/.match(path)
      end
    end
  end
end
