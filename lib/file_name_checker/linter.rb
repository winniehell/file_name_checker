require 'find'
require 'yaml'

# load available rules
Dir[File.expand_path('rules/*.rb', File.dirname(__FILE__))].each do |rule|
  require rule
end

module FileNameChecker
  class Linter
    DEFAULT_DIRECTORY_CONFIG = { 'ignore' => [ ], 'rules' => { } }

    attr_reader :has_violation
    alias_method :has_violation?, :has_violation

    def initialize(config_file_name = '.file_name_checker.yml')
      @config = YAML.load_file(config_file_name)
      @config['global_rules'] ||= { }
    end

    def run!
      @has_violation = false

      @config['directories'].each do |directory_path, directory_config|
        directory_config = DEFAULT_DIRECTORY_CONFIG.merge(directory_config || { })
        directory_config['ignore'] = directory_config['ignore'].map { |path| File.join(directory_path, path) }
        directory_config['rules'] = @config['global_rules'].merge(directory_config['rules'])

        rules = directory_config['rules'].map do |rule_name, rule_settings|
          rule_class = FileNameChecker.const_get(rule_name + 'Rule')
          rule_class.new(rule_settings)
        end

        Find.find(directory_path) do |path|
          rules.each do |rule|
            if directory_config['ignore'].include?(path)
              if FileTest.directory?(path)
                Find.prune
              else
                next
              end
            end

            if not rule.check(path)
              @has_violation = true
              puts "❌ #{path} violates #{rule}!"
            end
          end
        end
      end
    end
  end
end
