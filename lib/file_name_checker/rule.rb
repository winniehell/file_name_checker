module FileNameChecker
  class Rule
    def initialize(settings)
      @settings = settings
    end

    def to_s
      str = self.class.name.sub(/\A#{FileNameChecker.name}::/, '')
      
      if @settings
        str += @settings.to_s
      end

      str
    end
  end
end
